import { Log } from "../models/log.model"
import { Logger } from "../services/logging.service"
const config = require('../config')

const logSvc = new Logger()

const logErr: Log = new Log({
    service_name: config.service_name,
})

function logError(err, req, res, next) {
    console.error(err)
    logErr.status = err.statusCode || 500
    logErr.status_name = err.name
    logErr.message = err.message
    logSvc.error(logErr)
    next(err)
}

function returnError(err, req, res, next) {
    if (err.response) {
        err = err.response.data

        res.status(err.statusCode || 500).json({
            name: err.name,
            status: err.statusCode || 500,
            message: 'Responce err: ' + err.message
        })

    } else if (err.request) {
        err = err.request.data

        res.status(err.statusCode || 500).json({
            name: err.name,
            status: err.statusCode || 500,
            message: 'Request err: ' + err.message
        })

    } else {
        res.status(err.statusCode || 500).json({
            name: err.name,
            status: err.statusCode || 500,
            message: err.message
        })
    }
}

export { logError, returnError }
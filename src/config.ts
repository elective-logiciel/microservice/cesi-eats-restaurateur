const assert = require("assert");
const dotenv = require("dotenv");

// read in the .env file
dotenv.config();

// capture the environment variables the application needs
const { 
    PORT,
    HOST,
    SQL_SERVER,
    SQL_DATABASE,
    SQL_USER,
    SQL_PASSWORD,
    SQL_POOL_NAME,
    SQL_DRIVER,
    SQL_ENCRYPT,
    AUTH_SERVER_ADDRESS,
    AUTH_SERVER_PORT,
    LOG_SERVER_ADDRESS,
    LOG_SERVER_PORT,
    MICROSERVICE_NAME
} = process.env;

// const sqlEncrypt = process.env.SQL_ENCRYPT === "true";

// validate the required configuration information
assert(PORT, "PORT configuration is required.");
assert(HOST, "HOST configuration is required.");
assert(SQL_SERVER, "SQL_SERVER configuration is required.");
assert(SQL_DATABASE, "SQL_DATABASE configuration is required.");
assert(SQL_USER, "SQL_USER configuration is required.");
assert(SQL_PASSWORD, "SQL_PASSWORD configuration is required.");
assert(SQL_POOL_NAME, "SQL_EATS_NAME configuration is required.");
assert(SQL_DRIVER, "SQL_DRIVER configuration is required.");
assert(SQL_ENCRYPT, "SQL_ENCRYPT configuration is required.");
assert(AUTH_SERVER_ADDRESS, "AUTH_SERVER_ADDRESS configuration is required.");
assert(AUTH_SERVER_PORT, "AUTH_SERVER_PORT configuration is required.");
assert(LOG_SERVER_ADDRESS, "LOG_SERVER_ADDRESS configuration is required.");
assert(LOG_SERVER_PORT, "LOG_SERVER_PORT configuration is required.");
assert(MICROSERVICE_NAME, "MICROSERVICE_NAME configuration is required.");

// export the configuration information
module.exports = {
    service_name: MICROSERVICE_NAME,
    port: PORT,
    host: HOST,
    sql: {
        server: SQL_SERVER,
        database: SQL_DATABASE,
        user: SQL_USER,
        password: SQL_PASSWORD,
        dirver: SQL_DRIVER,
        pool_name: SQL_POOL_NAME,
        options: {
            encrypt: SQL_ENCRYPT
        }
    },
    auth: {
        address: AUTH_SERVER_ADDRESS,
        port: AUTH_SERVER_PORT,
    },
    log: {
        address: LOG_SERVER_ADDRESS,
        port: LOG_SERVER_PORT,
    }
};
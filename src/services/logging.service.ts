import { Log } from "../models/log.model";

const axios = require('axios');
const config = require('../config')

export class Logger {
    
    public async info(log: Log) {
        try {
            log.IsMessageValid()
            log.IsServiceNameValid()
            log.IsStatusNameValid()
            log.IsStatusValid()

            const server: string = config.log.address + ':' + config.log.port
            const url: string = 'http://' + server + '/logging/info'

            const options = {
                method: 'POST',
                url: url,
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    "message": log.message,
                    "status": log.status,
                    "status_name": log.status_name,
                    "service_name": log.service_name
                }
            };
            await axios.request(options)
            
        } catch (err) {
            console.log(err) 
        }
    }

    public async error(log: Log) {
        try {
            log.IsMessageValid()
            log.IsServiceNameValid()
            log.IsStatusNameValid()
            log.IsStatusValid()

            const server: string = config.log.address + ':' + config.log.port
            const url: string = 'http://' + server + '/logging/error'

            const options = {
                method: 'POST',
                url: url,
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    "message": log.message,
                    "status": log.status,
                    "status_name": log.status_name,
                    "service_name": log.service_name
                }
            };
            await axios.request(options)

        } catch (err) {
            console.log(err)
        }
    }
}

import { Token } from '../models/token.model';
// import { redirectError } from '../errors/redirectError';

const axios = require('axios');
const config = require('../config')

module.exports = async function VerifyJwt(req, res, next) {
    try {
        console.log('Request send:', req.originalUrl)

        if (req.originalUrl !== '/restaurateur/add' && req.originalUrl !== '/restaurateur/add/') {
            console.log('Checking token');

            const check_token: Token = new Token({
                id_user_token: req.body.id_user_token,
                token: req.headers.authorization,
            })

            check_token.IsIdUserValid()
            check_token.IsTokenValid()

            check_token.token = String(check_token.token).replace("Bearer ", "");

            const server: string = config.auth.address + ':' + config.auth.port
            const url: string = 'http://' + server + '/auth/verify'

            const options = {
                method: 'GET',
                url: url,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + check_token.token
                },
                data: {
                    "id_user": check_token.id_user_token
                }
            };

            var res = await axios.request(options)
            
            console.log('Token validated');
        }

        next()

    } catch (err) {
        next(err)
    }
}
import { Router } from 'express';
import { banking_detailsData } from '../datas/banking_details.data';
import { Banking_Details, convertQueryResToBankingDetailsList } from '../models/banking_details.model';
import { returnCreated, returnDeleted, returnSuccess, returnUpdated } from '../errors/success';
import { Error404 } from '../errors/errors';

const banckingDetailsController = Router();
const MssqlDB = new banking_detailsData();

banckingDetailsController.get('/:id_user(\\d+)', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl);

        const query_res = await MssqlDB.GetBankingDetailsByUserId(req.params.id_user);
        if (query_res.rowsAffected[0] === 0) {
            throw new Error404('Banking details not found, the user do not have banking details.')
        }

        const banking_details: Banking_Details = convertQueryResToBankingDetailsList(query_res)

        var message = "Get banking details by id_user"

        returnSuccess(res, banking_details, message)

    } catch (err) {
        next(err);
    }
})

banckingDetailsController.post('/add/', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl);

        const banking_details: Banking_Details = new Banking_Details({
            id_user: req.body.id_user,
            rib: req.body.rib,
        })

        banking_details.IsRibValid();
        banking_details.IsIdUserValid();

        await MssqlDB.InsertBankingDetails(banking_details);

        var message = "Banking details created"

        returnCreated(res, message)

    } catch (err) {
        next(err);
    }
})

banckingDetailsController.put('/update/rib/', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl);

        const banking_details: Banking_Details = new Banking_Details({
            id_user: req.body.id_user,
            rib: req.body.rib,
        })

        banking_details.IsRibValid();
        banking_details.IsIdUserValid();

        await MssqlDB.UpdateRibsByUserId(banking_details);

        var message = 'Banking details updated'

        returnUpdated(res, message)
        
    } catch (err) {
        next(err);
    }
})

banckingDetailsController.delete('/delete/:id_user(\\d+)', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl);

        await MssqlDB.DeleteRibByUserId(req.params.id_user);

        var message = 'banking details deleted'

        returnDeleted(res, message)
        
    } catch (err) {
        next(err);
    }
})

export { banckingDetailsController };
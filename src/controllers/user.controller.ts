import { Router } from 'express';
import { UserData } from '../datas/user.data';
import { User, convertQueryResToUser, convertQueryResToUserList } from '../models/user.model';
import { returnCreated, returnDeleted, returnSuccess, returnUpdated } from '../errors/success';
import { Error404, Error409 } from '../errors/errors';

const userController = Router();
const MssqlDB = new UserData();

userController.get('/:id_user(\\d+)', async(req, res, next)  =>{
    try {
        console.log('Request send:', req.originalUrl)

        const query_res = await MssqlDB.GetUserById(req.params.id_user)
        if (query_res.rowsAffected[0] === 0) {
            throw new Error404('User not found, this user does not exist.')
        }

        const user: User = convertQueryResToUser(query_res)

        var message = 'Get restaurateur bi id_user'

        returnSuccess(res, user, message);

    } catch (err) {
        next(err)
    }
});

// Get all restaurateur
userController.get('/all', async(req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const query_res = await MssqlDB.GetUsers()
        const user_list = convertQueryResToUserList(query_res)

        var message = 'Get all restaurateur'

        returnSuccess(res, user_list,message);

    } catch (err) {
        next(err)
    }
});

// Add restaurateur
userController.post('/add', async(req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const user: User =  new User({
            email: req.body.email,
            password: req.body.password,
            name: req.body.name,
            first_name: req.body.first_name,
        })

        var user_exits = await MssqlDB.IsUserExitsByEmail(user)
        if (user_exits) {
            throw new Error409("Email already used, can not create user.");
        }

        user.IsEmailValid()
        user.IsPasswordValid()
        user.IsNameValid()
        user.IsFirstNameValid()

        await MssqlDB.InsertUser(user)

        var message = 'Restaurateur created'

        returnCreated(res, message);
        
    } catch (err) {        
        next(err)
    }
})

// Delete restaurateur by user id
userController.delete('/delete/:id_user(\\d+)', async(req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        await MssqlDB.DeleteUserById(req.params.id_user)

        var message = 'Restaurateur deleted'

        returnDeleted(res, message);

    } catch (err) {
        next(err)
    }
})

// Update restaurateur email by user id
userController.put('/update/email', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        let user: User = new User({
            id_user: req.body.id_user,
            email: req.body.email,
        })

        user.IsIdUserValid()
        user.IsEmailValid()

        await MssqlDB.UpdateUserEmailById(user)

        var message = 'Email updated by id_user'

        returnUpdated(res, message);

    } catch (err) {
        next(err)
    }
})

// Update restaurateur password by user id
userController.put('/update/password', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        let user: User = new User({
            id_user: req.body.id_user,
            password: req.body.password,
        })

        user.IsIdUserValid()
        user.IsPasswordValid()

        await MssqlDB.UpdateUserPasswordById(user)

        var message = 'Password updated by id_user'

        returnUpdated(res, message);

    } catch (err) {
        next(err)
    }
})

// Update restaurateur name by user id 
userController.put('/update/name', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        let user: User = new User({
            id_user: req.body.id_user,
            name: req.body.name,
        })

        user.IsIdUserValid()
        user.IsNameValid()

        await MssqlDB.UpdateUserNameById(user)

        var message = 'Name updated by id_user'

        returnUpdated(res, message);
        
    } catch (err) {
        next(err)
    }
})

// Update restaurateur first name by user id
userController.put('/update/first_name', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        let user: User = new User({
            id_user: req.body.id_user,
            first_name: req.body.first_name,
        })

        user.IsIdUserValid()
        user.IsFirstNameValid()

        await MssqlDB.UpdateUserFirstNameById(user)

        var message = 'First name updated bu id_user'

        returnUpdated(res, message)

    } catch (err) {
        next(err)
    }
})

// Update restaurateur suspended by user id
userController.put('/update/suspended', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        let user: User = new User({
            id_user: req.body.id_user,
            suspended: req.body.suspended,
        })
        console.log(user);

        user.IsIdUserValid()
        user.IsSuspendedValid()

        await MssqlDB.UpdateUserSuspendedById(user)

        var message = 'Suspended status updated by id_user'

        returnUpdated(res, message)

    } catch (err) {
        next(err)
    }
})

// Update restaurateur parrain by user id
userController.put('/update/parrain',async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        let user: User = new User({
            id_user: req.body.id_user,
            id_parrain: req.body.id_parrain,
        })

        user.IsIdUserValid()
        user.IsIdParrainValid()

        await MssqlDB.UpdateIdParrainById(user)

        var message = 'Parrain updated by id_user'

        returnUpdated(res, message)

    } catch (err) {
        next(err)
    }
})

export { userController };
import { Router } from 'express';
import { AddressData } from '../datas/address.data';
import { Address, convertQueryResToAddressList } from '../models/address.model';
import { returnCreated, returnDeleted, returnSuccess, returnUpdated } from '../errors/success';
import { Error404 } from '../errors/errors';

const addressController = Router();
const MssqlDB = new AddressData();

// Get all address from a restaurateur with a user id
addressController.get('/:id_user(\\d+)', async function (req, res, next) {
    try {
        console.log('Request send:', req.originalUrl)

        const query_res = await MssqlDB.GetAddressByUserId(req.params.id_user)
        if (query_res.rowsAffected[0] === 0) {
            throw new Error404('Address not found, the user do not have an address.')
        }
        
        const address: Address = convertQueryResToAddressList(query_res)

        var message = 'Get address by id_user'

        returnSuccess(res, address, message);

    } catch (err) {
        next(err)
    }  
})

// Add address with user id
addressController.post('/add/',async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        const address: Address = new Address({
            street: req.body.street,
            zip_code: req.body.zip_code,
            city: req.body.city,
            id_user: req.body.id_user,
        })

        address.IsStreetValid()
        address.IsZipCodeValid()
        address.IsCityValid()
        address.IsIdUserValid()

        await MssqlDB.InsertAddress(address)

        var message = 'Address created'

        returnCreated(res, message)
        
    } catch (err) {
        next(err)
    }
})

// Delete address by address id
addressController.delete('/delete/:id_address(\\d+)', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        await MssqlDB.DeleteAddressById(req.params.id_address)

        var message = 'Address deleted by id_address'

        returnDeleted(res, message)

    } catch (err) {
        next(err)
    }
})

// Delete all address by user id
addressController.delete('/delete/all/:id_user(\\d+)', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        await MssqlDB.DeleteAllAddressByIdUser(req.params.id_user)

        var message = 'address deleted by id_user'

        returnDeleted(res, message)

    } catch (err) {
        next(err)
    }
})

// Update adress street by address id
addressController.put('/update/street', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        let address: Address = new Address({
            street: req.body.street,
            id_address: req.body.id_address,
        })

        address.IsStreetValid()
        address.IsIdAddressValid()

        await MssqlDB.UpdateAddressStreetById(address)

        var message = 'Street updated'

        returnUpdated(res, message)

    } catch (err) {
        next(err)
    }
})

// Update address zip code by address id
addressController.put('/update/zip_code', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        let address: Address = new Address({
            zip_code: req.body.zip_code,
            id_address: req.body.id_address,
        })

        address.IsZipCodeValid()
        address.IsIdAddressValid()

        await MssqlDB.UpdateAddressZipCodeById(address)

        var message = 'Updated zip code'

        returnUpdated(res, message)

    } catch (err) {
        next(err)
    }
})

// Update restaurateur city by address id
addressController.put('/update/city', async (req, res, next) => {
    try {
        console.log('Request send:', req.originalUrl)

        let address: Address = new Address({
            city: req.body.city,
            id_address: req.body.id_address,
        })

        address.IsCityValid()
        address.IsIdAddressValid()

        await MssqlDB.UpdateAddressCityById(address)

        var message = 'City updated'

        returnUpdated(res, message);

    } catch (err) {
        next(err)
    }
})


export { addressController };
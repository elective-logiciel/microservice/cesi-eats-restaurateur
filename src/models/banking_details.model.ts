import { Error400 } from "../errors/errors"

export class Banking_Details {
    id_banking_details?: number
    rib?: string
    id_user?: number

    public constructor(init?: Partial<Banking_Details>) {
        Object.assign(this, init);
    }

    public IsIdABankingDetailsValid(): boolean {
        if (this.id_banking_details === undefined) {
            throw new Error400("Missing argument, 'id_banking_details' can not be NULL")
        }
        if (this.id_banking_details < 0) {
            throw new Error400("Out of range argument, 'id_banking_details' can not be a negative number")
        }
        return true
    }

    public IsRibValid(): boolean {
        if (this.rib === undefined) {
            throw new Error400("Missing argument, 'rib' can not be NULL")
        }
        if (this.rib === "") {
            throw new Error400("Empty argument, 'rib' can not be EMPTY")
        }
        return true
    }

    public IsIdUserValid(): boolean {
        if (this.id_user === undefined) {
            throw new Error400("Missing argument, 'id_user' can not be NULL")
        }
        if (this.id_user < 0) {
            throw new Error400("Out of range argument, 'id_user' can not be a negative number")
        }
        return true
    }
}

// Convert the result send by the database into a array of user struct
export function convertQueryResToBankingDetailsList(query_res: any): Banking_Details {

    const res_banking_details: any = query_res.recordset[0]

    return new Banking_Details({
            id_banking_details: res_banking_details.Id_Banking_Details,
            rib: res_banking_details.Rib,
            id_user: res_banking_details.Id_User,
        })
}
import express from 'express'
import { userController } from './controllers/user.controller'
import { addressController } from './controllers/address.controller'
import { banckingDetailsController } from './controllers/banking_details.controller'
import { Error404, Error408 } from './errors/errors'

const { logError, returnError } = require('./errors/errorHandler')
// const config = require('./config')
// var timeout = require('connect-timeout');
const jwtMiddleware = require('./middlewares/jwt.middleware')

/**
 * App creation
 */
const app = express()

/**
 * Deffining requets parsing
 */
app.use(express.json())

/**
 * Logger
 */

/**
 * Time-out definition
 */
// app.use(timeout(120000));
// app.use(haltOnTimedout);

// /**
//  * Authorization to connect to this api
//  */
// app.use(cors())

/**
 * Middleware
 */
app.use(jwtMiddleware)

/**
 * Route handling
 */
app.use('/restaurateur', userController)
app.use('/restaurateur/address', addressController)
app.use('/restaurateur/banking_details', banckingDetailsController)

// default route
app.use('*', (req, res, next) => {
    throw new Error404('Invalid route, "' + req.originalUrl + '" does not exist. If your are confidient in the your url, maibe the type of request is wrong (get, post, ...).')
})

/**
 *  Time-out handling
 */
// function haltOnTimedout(req, res, next){
//     if (req.timedout) {
//         next()
//     }
//     throw new Error408('Route "' + req.originalUrl + '" does not resolve in time.')
// }

/**
 * Root page
 */
// app.get('/', (req, res) => res.send('hello world'))

/**
 * Errors handling
 */
app.use(logError)
app.use(returnError)

/**
 * export application config
 */
module.exports = app;

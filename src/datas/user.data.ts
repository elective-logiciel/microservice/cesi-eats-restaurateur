import { User } from "../models/user.model"
import { mssql, sqlConfig } from "../services/mssql.service"
const sql = require('mssql')

const mssqlDB = new mssql();

export class UserData {

    public async GetUserById(id_user: number) {
        const pool = await await mssqlDB.get(sqlConfig.name, sqlConfig.config)

        var request = pool.request()

        request.input('id_user', sql.Int, id_user)

        const res = await request.query('SELECT Id_Users, Email, Name, First_Name, Id_Parrain, Suspended FROM Users WHERE Id_Users = @id_user AND Id_Types = 4')
        console.log(res);
        pool.close()

        return res
    }

    public async GetUsers() {
        const pool = await mssqlDB.get(sqlConfig.name, sqlConfig.config)

        var request = pool.request()

        const res = await request.query('SELECT Id_Users, Email, Name, First_Name, Id_Parrain, Suspended FROM Users WHERE Id_Types = 4')
        
        pool.close()

        return res
    }

    public async IsUserExitsByEmail(user: User): Promise<boolean> {
        const pool = await mssqlDB.get(sqlConfig.name, sqlConfig.config)

        var request = pool.request()

        request.input('email', sql.NVarChar, user.email)

        const res = await request.query('SELECT Id_Users FROM Users WHERE Email = @email')

        pool.close()

        if (res.rowsAffected[0] > 0) {
            return true
        }

        return false
    }

    public async InsertUser(user: User) {
        var pool = await mssqlDB.get(sqlConfig.name, sqlConfig.config)

        var request = pool.request()

        request.input('email', sql.NVarChar, user.email)
        request.input('password', sql.NVarChar, user.password)
        request.input('name', sql.NVarChar, user.name)
        request.input('first_name', sql.NVarChar, user.first_name)

        await request.query('INSERT INTO Users (Email, Password, Name, First_Name, Suspended, Id_Types) VALUES (@email, @password, @name, @first_name, 0, 4)')

        pool.close()

    }

    public async DeleteUserById(id_user: number) {
        var pool = await mssqlDB.get(sqlConfig.name, sqlConfig.config)

        var request = pool.request()

        request.input('id_user', sql.Int, id_user)

        await request.query('DELETE FROM Users WHERE Id_Users = @id_user')

        pool.close()
    }

    public async UpdateUserEmailById(user: User) {
        var pool = await mssqlDB.get(sqlConfig.name, sqlConfig.config)

        var request = pool.request()

        request.input('id_user', sql.Int, user.id_user)
        request.input('email', sql.NVarChar, user.email)

        await request.query('UPDATE Users SET Email = @email WHERE Id_Users = @id_user')

        pool.close()
    }

    public async UpdateUserPasswordById(user: User) {
        var pool = await mssqlDB.get(sqlConfig.name, sqlConfig.config)

        var request = pool.request()

        request.input('id_user', sql.Int, user.id_user)
        request.input('password', sql.NVarChar, user.password)

        await request.query('UPDATE Users SET Password = @password WHERE Id_Users = @id_user')

        pool.close()
    }

    public async UpdateUserNameById(user: User) {
        var pool = await mssqlDB.get(sqlConfig.name, sqlConfig.config)
        
        var request = pool.request()

        request.input('id_user', sql.Int, user.id_user)
        request.input('name', sql.NVarChar, user.name)

        await request.query('UPDATE Users SET Name = @name WHERE Id_Users = @id_user')

        pool.close()
    }

    public async UpdateUserFirstNameById(user: User) {
        var pool = await mssqlDB.get(sqlConfig.name, sqlConfig.config)

        var request = pool.request()

        request.input('id_user', sql.Int, user.id_user)
        request.input('first_name', sql.NVarChar, user.first_name)

        await request.query('UPDATE Users SET First_Name = @first_name WHERE Id_Users = @id_user')

        pool.close()
    }

    public async UpdateUserSuspendedById(user: User) {
        var pool = await mssqlDB.get(sqlConfig.name, sqlConfig.config)

        var request = pool.request()

        var suspended: number = (user.suspended) ? 1 : 0

        request.input('id_user', sql.Int, user.id_user)
        request.input('suspended', sql.Int, suspended)

        await request.query('UPDATE Users SET Suspended = @suspended WHERE Id_Users = @id_user')

        pool.close()
    }

    public async UpdateIdParrainById(user: User) {
        var pool = await mssqlDB.get(sqlConfig.name, sqlConfig.config)

        var request = pool.request()

        request.input('id_user', sql.Int, user.id_user)
        request.input('id_parrain', sql.Int, user.id_parrain)

        await request.query('UPDATE Users SET Id_Parrain = @id_parrain WHERE Id_Users = @id_user')

        pool.close()
    }
}